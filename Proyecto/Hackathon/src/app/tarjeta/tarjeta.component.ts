import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-tarjeta',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './tarjeta.component.html',
  styleUrl: './tarjeta.component.css'
})
export class TarjetaComponent {

  encontrar: any;
  numero: number=1;

  visible=false;
  visible1=false;

  constructor(private apiService:ApiService){
    this.encontrar=apiService.encontrarUsuario(this.numero)
    .subscribe(usuarios => this.encontrar=usuarios);
  }

  siguiente(){
    this.numero=1;
    this.encontrar=this.apiService.encontrarUsuario(this.numero)
    .subscribe(usuarios => this.encontrar=usuarios);

  }


  anterior(){
    this.numero=2;
    this.encontrar=this.apiService.encontrarUsuario(this.numero)
    .subscribe(usuarios => this.encontrar=usuarios);

  }

  evento(){
    this.visible=!this.visible;
   }

   evento1(){
    this.visible1=!this.visible1;
   }



}
