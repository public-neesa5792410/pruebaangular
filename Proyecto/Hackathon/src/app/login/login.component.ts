import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { ApiService } from '../api.service';
import { RouterOutlet, RouterLink,  Router } from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule,RouterLink],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  usuario: any;

  encontrar: any;

  constructor(private apiService:ApiService, private router: Router){
    //this.encontrar=apiService.encontrarUsuario()
    //.subscribe(usuarios => this.encontrar=usuarios);
  }

  formularioLogin = new FormGroup({

    usuario: new FormControl(''),
    password: new FormControl(''),
  });

  submit() {
    this.usuario ={
      email: this.formularioLogin.value.usuario,
      password: this.formularioLogin.value.password,
    }
    this.apiService.buscarUsuario(this.usuario);

  if(localStorage.getItem("usuario")!="" ){
      this.router.navigate(['/fotos']);
    }else{
      alert("Usuario no encontrado");
    }
  }


}
