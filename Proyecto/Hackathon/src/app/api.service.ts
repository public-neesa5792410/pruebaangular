import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  token!: any;

  buscarUsuario(usuario: any){
    let jsonData = JSON.stringify(usuario);
    console.log(jsonData)
    return this.http.post("https://reqres.in/api/api/login", jsonData)
    .subscribe(
      response=> {
        //console.log("usuario" + response);
        this.token=response;

        localStorage.setItem("usuario", this.token)
        alert(JSON.stringify(localStorage.getItem("usuario")
        ))
      },

      error =>{
        console.log("Error " + error);
      }


    )

  }


  encontrarUsuario(num:number){
      return this.http.get("https://reqres.in/api/users?page="+num);
  }



  }


