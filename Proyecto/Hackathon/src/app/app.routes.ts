import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TarjetaComponent } from './tarjeta/tarjeta.component';


export const routes: Routes = [
  {
    path:"login",
    component:LoginComponent
  },

  {
    path:"fotos",
    component: TarjetaComponent
  }
];
